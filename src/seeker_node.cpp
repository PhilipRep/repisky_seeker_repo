#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"
#include "std_srvs/SetBool.h"
#include "geometry_msgs/Twist.h"

// This file is used to localize and navigate towards a static, unobstructed ball
// in an open environment.
// A boolean service is set to determine its run condition, set by a service call
// If set to "true", a subscriber to the /scan data determines the location of a
// ball in the scene.
// Once determined where in the scan it is located, the error between that angle
// and the forward direction of the robot is minimized by the controller.
// At a preset threshold, the controller will then also apply a forward command.
// While closing the gap, the angle and displacement are converted to a Cartesian
// representation of the relative distance to target.
// (+) x is forward, (+) y is right, (+) z is up.
// Once under 0.5 meters away, the forward command is halted and the bot waits
// for any change in the scene.


class Seeker
{

public:

  // second nodehandle for publishing
  ros::NodeHandle nh2_;
  // controller publisher for turtlebot
  ros::Publisher control_pub_;
  // displacement publisher
  ros::Publisher disp_pub_;
  // service call request
  bool resp_;
  // vector of all locations of non-nan returns
  std::vector<uint16_t> angles;
  // vector of all distances to non-nan returns
  std::vector<float> ang_dist;
  // value of angular error in robot
  float error;

  //Constructor to take NodeHandle for publisher/subscriber
  Seeker()
  {
    // init false
    resp_ = false;

    // init publisher for turtlebot
    control_pub_ = nh2_.advertise<geometry_msgs::Twist>
      ("/mobile_base/commands/velocity",10);
    // init publisher for relative displacement to ball
    disp_pub_ = nh2_.advertise<geometry_msgs::Vector3>("displacement",10);
  }

  // callback for enable service
  bool enableCb(std_srvs::SetBool::Request& request,
                std_srvs::SetBool::Response& response)
  {
    response.success = request.data;
    // update param
    resp_ = request.data;
    return true;
  }

  // function to locate ball angle
  void findBall(const sensor_msgs::LaserScan::ConstPtr& scan_in)
  {

    // vector of all range returns (nan if blank)
    std::vector<float> ranges = scan_in->ranges;

    // should be 639 scans (639 on left, 0 on right)
    uint16_t scans = ranges.size();

    // need to sort through vector to find ball returns
    for(int ii=0; ii < scans; ii++)
      {
        // nan != nan, filter for positions of ball
        if(ranges[ii] == ranges[ii])
          {
            // add to vector angle and distance
            angles.push_back(ii);
            ang_dist.push_back(ranges[ii]);
          };
      }

    // bamboozle
    if(ang_dist.empty())
      {
        error = 0.0;
      }
    else
      {
        // need the middle scan value -> also shortest distance
        uint16_t size_ang = angles.size()/2;

        // report error as fraction of value, arbitrary multiplier
        error = ( angles[size_ang] - 320.0 ) / 1280.0 ;
      }
  }

  // function to move towards ball
  void findController()
  {

    // init controller message
    geometry_msgs::Twist control_msg;

    // need the middle scan value -> also shortest distance
    uint16_t size_ang = angles.size()/2;

    // correct for misalignment
    control_msg.angular.z = error;

    // enter x linear motion to mobile base
    if(abs(error) < 0.05 && ang_dist[size_ang] > 0.48)
      {
        control_msg.linear.x = 0.15;
      }
    else
      {
        control_msg.linear.x = 0.0;
      }

    //send command to move
    control_pub_.publish(control_msg);
  }

  // function to report x,y,z displacement
  void findDisp()
  {
    // set controller message
    geometry_msgs::Vector3 disp_msg;

    // hypotenuse is the line of sight for the beam
    float hyp = ang_dist[ang_dist.size()/2];

    // angle is the # scans over from center * the angle increment
    float ang = (320.0 - angles[ang_dist.size()/2]) * 0.00163668883033;

    // convert to relative Cartesian
    float y_dist = std::sin(ang) * hyp;
    float x_dist = std::cos(ang) * hyp;

    // enter x/y displacement from ball
    disp_msg.x = x_dist;
    disp_msg.y = y_dist;

    // send displacement to subscribers
    disp_pub_.publish(disp_msg);
  }

  // callback for laserscan
  void callback(const sensor_msgs::LaserScan::ConstPtr& scan_in)
  {

    // decide whether to run program
    if(resp_)
      {
        // function call to scan filter
        this->Seeker::findBall(scan_in);

        // abort
        if(angles.empty())
          {
            // nothing to see here!
          }
        // run normal
        else
          {
            //function to move bot
            this->Seeker::findController();

            // function to find relative displacement
            this->Seeker::findDisp();

            //reset angles and ranges
            angles.clear();
            ang_dist.clear();

          }
      }
  }
};

int main(int argc, char **argv)
{
  ros::init(argc, argv, "seeker_node");
  ros::NodeHandle nh;

  // instantiate class
  Seeker node;

  // init callbcak for laserscan data
  ros::Subscriber sub = nh.subscribe("scan", 10, &Seeker::callback, &node);

  // init service for enable/disable
  ros::ServiceServer service = nh.advertiseService("enable", &Seeker::enableCb,
                                                   &node);

  // allow multiple callbacks
  ros::MultiThreadedSpinner s1(2);

  // start processes
  ros::spin(s1);

  return 0;
}
