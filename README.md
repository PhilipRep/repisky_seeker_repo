## Challenge

use turtle bot (roomba/kinect) to find and move towards ball in scene

The behavior should not contain any pre-scripted motions and should work for any placement of the ball as long as the ball is within the detection range of the kinect.

### Assumptions

Single, static, unobscured ball is present in FOV

### Nodes

seeker_node -> find ball in scene

### constraints

ball-seeker off --> std_srvs/SetBool ROS service call

relative distance to ball --> geometry_msgs/Vector3

ball within range of kinect (location unknown)

package name --> "seeker"

displacement topic --> "displacement"

enable service --> "enable"

node name --> "seeker_node"


### running

sudo apt install ros-kinetic-turtlebot-msgs

1) catkin_make

2) roslaunch turtlebot_gazebo turtlebot_world.launch world_file:='"<path>/mini_world.world"'

3) roslaunch seeker interview.launch

4) rosservice call /enable “data: true”

5) change ball configuration

6) echo realtive displacement through dispacement topic

### Env

1) Ubuntu/ROS

2) sudo apt-get -y install ros-kinetic-turtlebot-gazebo ros-kinetic-desktop-full ros-kinetic-turtlebot-rviz-launchers ros-kinetic-warehouse-ros ros-kinetic-gazebo-plugins

3) roslaunch turtlebot_gazebo turtlebot_world.launch world_file:='"<path>/mini_world.world"'

3.5) or

rostopic pub /cmd_vel_mux/input/teleop geometry_msgs/Twist '[-2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

roslaunch turtlebot_gazebo turtlebot_world.launch

roslaunch turtlebot_teleop keyboard_teleop.launch

roslaunch turtlebot_rviz_launchers view_robot.launch


4) create package -->  catkin_create_pkg seeker geometry_msgs sensor_msgs std_msgs std_srvs roscpp

Identifier : Case

Namespace: lower_with_underscores

Enum: PascalCase

Enum Value: UPPERCASE_WITH_UNDERSCORES

Struct: PascalCase

Struct Method: camelCase

Class: PascalCase

Class Method: camelCase

Function: camelCase

Parameter: lower_with_underscores

Member Variable: lower_with_underscores

* Should start with m_ unless part of a simple struct or private class

* One exception is taus::StatusReporter status IDs. These are UCASE_W_UNDER

Function Variable: lower_with_underscores

Constants: lower_with_underscores

Macros/defines: UPPERCASE_WITH_UNDERSCORES
